create table customer (
  id varchar (36),
  customer_number varchar (100) not null,
  name varchar (255) not null,
  email varchar (255) not null,
  mobile_phone varchar(100) not null,
  primary key (id),
  unique (customer_number)
);

create table invoice (
  id varchar (36),
  id_customer varchar(36) not null,
  invoice_number varchar (100) not null,
  create_date DATE not null,
  description varchar(255) not null,
  amount decimal(19,2) not null,
  paid boolean not null default false,
  primary key (id),
  foreign key (id_customer) references customer(id),
  unique (invoice_number)
);
